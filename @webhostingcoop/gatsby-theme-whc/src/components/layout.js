import React from 'react'
import { Heading, Container } from "theme-ui"
import TopBar from "./topbar"
import NavBar from "./navbar"
import Footer from "./footer"
import '../styles/whc.sass'

const Layout = ({ children }) => {
  return (
  <div>
    <TopBar></TopBar>
    <Container>{children}</Container>
    <Footer></Footer>
  </div>
  )
}

export default Layout
