import React from "react"
import { graphql } from "gatsby"
import Link from 'gatsby-plugin-transition-link'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Img from "gatsby-image"
import { IconContext } from "react-icons";
import { FaChevronRight } from 'react-icons/fa';
import { FaHeart } from 'react-icons/fa';
import foundationFooterLogo from '../images/foundation-footer-logo.png';

export default ({data}) => (
<>
<footer>
<section id="footer" className="footer-sec">
<div className="container">
    <div className="row">
        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
          <div id="foundationFooterLogo">
            <a href="/">
              <img src={foundationFooterLogo} alt="Foundation Footer Logo" />
            </a>
          </div>
        </div>
        <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 footer-links">
            <ul>
                <li><a href="./foundation" title="Foundation">Foundation</a></li>
                <li><a href="./benefits" title="Benefits">Benefits</a></li>
                <li><a href="./cooperative-associations" title="Cooperative Associations">Coops</a></li>
                <li><a href="./non-profit-corporations" title="Non-Profit Corporations">Non-Profits</a></li>
                <li><a href="./support-cause" title="Support WebHosting.coop Foundation">Support Cause</a></li>
            </ul>
        </div>
    </div>
</div>
</section>
</footer>
</>
)

export const query = graphql`
query {
  imageOne: file(relativePath: { eq: "foundation-footer-logo.png" }) {
    childImageSharp {
      fluid(maxWidth: 1000) {
        base64
        tracedSVG
        aspectRatio
        src
        srcSet
        srcWebp
        srcSetWebp
        sizes
        originalImg
        originalName
      }
    }
  }
}
`
