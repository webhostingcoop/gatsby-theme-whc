import React from "react"
import Link from 'gatsby-plugin-transition-link'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import { IconContext } from "react-icons";
import { FaFacebook } from 'react-icons/fa';
import { FaTwitter } from 'react-icons/fa';
import { FaGithub } from 'react-icons/fa';
import { FaLinkedin } from 'react-icons/fa';

const TopBar = ({ events }) => {
  return (
    <>
      <div id="uberid">
            <section id="topbar" className="section">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12 social left">
                          <div class="domainform">
                                   <form method="post" action="https://dashboard.webhosting.coop/domainchecker.php" id="frmDomainHomepage">
                                        <input type="hidden" name="token" value="7d291e11928ab0309df5273434b1781f62f8132c"/>
                                              <div class="input-group input-group-lg">
                                                <input type="text" class="form-control main-input" name="domain" placeholder="eg. example.com" autocapitalize="none" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="" data-original-title="Required"/>
                                                <span class="input-group-btn">
                                                    <input type="submit" class="btn search" value="Search"/>
                                                </span>
                                              </div>
                                  </form>
                          </div>
                        </div>
                        <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12 support right">
                            <ul>
                                <li className="top-txt"><a href="mailto:coopadmin@webhosting.coop">Contact us</a></li>
                                <li className="top-divider">/</li>
                                <li className="top-txt"><a href="tel:8555122667">855.512.2667 (COOP)</a></li>
                                <li className="top-divider">| |</li>
                                <AniLink
                                  fade
                                  to="/student-signup"
                                  state={{modal: true}}
                                  className="top-button"
                                >
                                  <button type="button"
                                    className="btn btn-topbar1 btn-c"
                                    data-toggle="modal"
                                    data-target=".student-signup">
                                      Get Started
                                      <i className="fa fa-chevron-right"></i>
                                  </button>
                                </AniLink>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
      </div>
    </>
  )
}

export default TopBar
