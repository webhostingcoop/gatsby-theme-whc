import React from 'react'
import { Heading, Container } from "theme-ui"
import TopBar from "./topbar"
import NavBar from "./navbar"

const Layout = ({ children }) => {
  return (
  <div>
    <TopBar></TopBar>
    <NavBar></NavBar>
    <Container>{children}</Container>
  </div>
  )
}

export default Layout
