import React from "react"
import { graphql } from "gatsby"
import Link from 'gatsby-plugin-transition-link'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Img from "gatsby-image"
import { IconContext } from "react-icons";
import { FaChevronRight } from 'react-icons/fa';
import { FaHeart } from 'react-icons/fa';

const NavBar = ({data}) => (
<section id="navigation" class="section">
<div class="navbar yamm navbar-default ">
      <div class="container">
        <div class="row">
         <div id="logo">
           <a href="/" class="standard-logo"><Img fluid={data.logo.childImageSharp.fluid} alt="" /></a>
         </div>
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div id="navbar-collapse-1" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Classes <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li>
                  <div class="yamm-content">
                    <div class="row">
                      <div class="col-lg-5ths col-md-5ths col-sm-5ths nav-col">
                        <h2><a href="./website-classes">Websites</a></h2>
                        <div><a href="./website-classes"><img class="nav1" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGAQMAAABL4HDHAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA9JREFUeNpjYBgFo2BoAgACvAABARqrRwAAAABJRU5ErkJggg=="></img></a></div>
                        <p>Get a website online and gain wisdom from real-life development.</p>
                        <div><button type="button" onclick="window.location.href='./website-classes'" class="btn btn-ghost btn-e">Learn More <i class="fa fa-chevron-right"></i></button></div>
                        </div>
                      <div class="col-lg-5ths col-md-5ths col-sm-5ths nav-col">
                        <h2><a href="./webhosting-classes">Web Hosting</a></h2>
                        <div><a href="./webhosting-classes"><img class="nav2" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGAQMAAABL4HDHAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA9JREFUeNpjYBgFo2BoAgACvAABARqrRwAAAABJRU5ErkJggg=="></img></a></div>
                        <p>Understand how the Internet works and understand it’s ups and downs.</p>
                        <div><button type="button" onclick="window.location.href='./webhosting-classes'" class="btn btn-ghost btn-e">Learn More <i class="fa fa-chevron-right"></i></button></div>
                        </div>
                      <div class="col-lg-5ths col-md-5ths col-sm-5ths nav-col">
                           <h2><a href="./administration-classes">Administration</a></h2>
                           <div><a href="./administration-classes"><img class="nav3" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGAQMAAABL4HDHAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA9JREFUeNpjYBgFo2BoAgACvAABARqrRwAAAABJRU5ErkJggg=="></img></a></div>
                        <p>Manage your own server and learn how to become a Administrator.</p>
                        <div><button type="button" onclick="window.location.href='./administration-classes'" class="btn btn-ghost btn-e">Learn More <i class="fa fa-chevron-right"></i></button></div>
                        </div>
                      <div class="col-lg-5ths col-md-5ths col-sm-5ths nav-col">
                        <h2><a href="./development-classes">Development</a></h2>
                        <div><a href="./development-classes"><img class="nav4" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGAQMAAABL4HDHAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA9JREFUeNpjYBgFo2BoAgACvAABARqrRwAAAABJRU5ErkJggg=="></img></a></div>
                        <p>Innovate and create applications to solve real world issues.</p>
                        <div><button type="button" onclick="window.location.href='./development-classes'" class="btn btn-ghost btn-e">Learn More <i class="fa fa-chevron-right"></i></button></div>
                        </div>
                      <div class="col-lg-5ths col-md-5ths col-sm-5ths  nav-col" style={{borderRight: 'none'}}>
                         <h2><a href="./webdesign-classes">Web Design</a></h2>
                        <div><a href="./webdesign-classes"><img class="nav5" alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGAQMAAABL4HDHAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAA9JREFUeNpjYBgFo2BoAgACvAABARqrRwAAAABJRU5ErkJggg=="></img></a></div>
                        <p>Design solutions to UX issues and expand your creativity in the process.</p>
                        <div><button type="button" onclick="window.location.href='./webdesign-classes'" class="btn btn-ghost btn-e">Learn More <i class="fa fa-chevron-right"></i></button></div>
                        </div>
                    </div>
                  </div>
                </li>
              </ul>
            </li>
              <li class="yamm-fw"><a href="./foundation">Foundation</a></li>
              <li class="yamm-fw"><a href="./benefits">Benefits</a></li>
          </ul>
        </div>
               <div class="support-button"> <button type="button" class="btn btn-contrast btn-nav" onclick="window.location.href='./support-cause'">Support Cause <i class="fa fa-heart-o"></i></button>
            </div>
        </div>
    </div>
</div><div style={{ clear: 'both'}}></div>
</section>
)

export const fluidImage = graphql`
  fragment fluidImage on File {
    childImageSharp {
      fluid(maxWidth: 1000) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`;

export const pageQuery = graphql`
  query {
    teacher: file(relativePath: { eq: "teacher.png" }) {
      ...fluidImage
    }
    logo: file(relativePath: { eq: "logo.png" }) {
      ...fluidImage
    }
  }
`

export default NavBar
