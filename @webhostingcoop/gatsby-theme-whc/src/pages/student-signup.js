import React from 'react'
import { Link } from 'gatsby'
import Img from "gatsby-image"
import { ModalRoutingContext } from 'gatsby-plugin-modal-routing'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import { IconContext } from "react-icons";
import { FaTimesCircle } from 'react-icons/fa';

const StudentSignupPage = props => (
  <ModalRoutingContext.Consumer>
    {({ modal, closeTo }) => (
      <div>
<section id="inc-get-started" className="section">
<div className="student-signup" role="dialog" >
  <div className="modal-dialog modal-lg">
    <div className="modal-content">
          <div className="close2">
          <AniLink
            fade
            to="/"
            state={{
              modal: true
            }}
          >
          <IconContext.Provider value={{ color: "red", className: "fa fa-times-circle red" }}>
            <div>
              <FaTimesCircle />
            </div>
          </IconContext.Provider>
          </AniLink>
          </div>
            <div className="row">
                <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                    <div className="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                      <div className="line2"><h2 style={{marginTop: '40px',marginRight: '0',marginBottom: '20px',marginLeft: '0'}}><span>Get Started Today!</span></h2></div>
                        <p className="text-center">We have over 18 classes to choose from. If you are a beginner and want to learn how to get a website online or an advanced user wanting to learn how to administrate a web server, we have the class for you. All classes are free of charge. We do accept donations. </p>
                    </div>
                <div className="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
            </div>

            <div className="row modal-sec">
                <div className="col-lg-1 col-md-1 col-sm-hidden col-xs-hidden"></div>
                <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 center">
                  <Img fluid={props.data.teacher.childImageSharp.fluid} alt="" />
                </div>
                    <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12 form-content">
                         <form className="cmxform" id="commentForm" name="contactform" method="post" action="thank-you.php">
                        <div className="row ">
                            <div className="col-lg-10 col-md-10  form-row">
                                <div className='input-container'>
                                    <div className="icon-ph"><i className="fa fa-user"></i></div>
                                    <input id="cname" name="first_name" minLength="2" className="form-name" type="text" placeholder="Name or Organization" required ></input>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-2"></div>
                        </div>
                        <div className="row">
                            <div className="col-lg-5 col-md-5 form-row two-line-left" >
                                <div className='input-container'>
                                    <div className="icon-ph"><i className="fa fa-envelope"></i></div>
                                    <input id="cemail" type="email" name="email" className="form-email" placeholder="Email Address" required ></input>
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-5 form-row two-line-right" >
                                <div className='input-container'>
                                    <div className="icon-ph"><i className="fa fa-mobile-phone"></i></div>
                                    <input id="phone" name="telephone" className="form-phone" type="text" placeholder="Phone Number" required></input>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-2"></div>
                        </div>
                             <div className="row">
                            <div className="col-lg-10 col-md-10 form-row">
                                <div className='input-container'>
                                    <select className="form-control" name="classsubject">
                                        <option>-- Select Class --</option>
                                        <optgroup label="Websites">
                                            <option value="Introduction to Websites, Email, and Web Hosting">Introduction to Websites, Email, and Web Hosting</option>
                                            <option value="Introduction to Website Applications">Introduction to Website Applications</option>
                                            <option value="Introduction to E-commerce">Introduction to E-commerce</option>
                                        </optgroup>
                                        <optgroup label="Web Hosting">
                                            <option value="Introduction to Web Hosting">Introduction to Web Hosting</option>
                                            <option value="Understanding Web Servers">Understanding Web Servers</option>
                                            <option value="Deep Dive into Domains">Deep Dive into Domains</option>
                                        </optgroup>
                                        <optgroup label="Server Administration">
                                            <option value="Software Patching and Maintenance">Software Patching and Maintenance</option>
                                            <option value="Server Stability and Load Mitigation">Server Stability and Load Mitigation</option>
                                            <option value="Security and Attack Prevention">Security and Attack Prevention</option>
                                        </optgroup>
                                        <optgroup label="Development">
                                            <option value="Introduction to GIT and GITHUB">Introduction to GIT and GITHUB</option>
                                            <option value="Programing Languages and Frameworks">Programing Languages and Frameworks</option>
                                            <option value="Databases">Databases</option>
                                        </optgroup>
                                        <optgroup label="Web Design">
                                            <option value="Introduction to Visual Design">Introduction to Visual Design</option>
                                            <option value="Introduction to Interaction Design">Introduction to Interaction Design</option>
                                            <option value="Introduction to User Experience Research">Introduction to User Experience Research</option>
                                        </optgroup>
                                        <optgroup label="Sociology">
                                            <option value="Social Businesses">Social Businesses</option>
                                            <option value="Social Relations">Social Relations</option>
                                            <option value="Technology and Society">Technology and Society</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                                <div className="col-lg-2 col-md-2"></div>
                             </div>
                       <div className="row">
                           <div className="col-lg-1 col-md-1"></div>
                               <div className="col-lg-8 col-md-8 center register-button">
                                  <input className="submit" type="submit" name="submit" value="Register Now!"></input>
                               </div>
                            <div className="col-lg-3 col-md-3"></div>
                           </div>
                        </form>
                    </div>
            </div>
            </div>
    </div>
</div>
</section>
      </div>
    )}
  </ModalRoutingContext.Consumer>
)

export default StudentSignupPage

export const fluidImage = graphql`
  fragment fluidImage on File {
    childImageSharp {
      fluid(maxWidth: 1000) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`;

export const pageQuery = graphql`
  query {
    teacher: file(relativePath: { eq: "teacher.png" }) {
      ...fluidImage
    }
  }
`
