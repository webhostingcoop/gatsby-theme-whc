import React, { Component } from 'react'
import Img from "gatsby-image"
import gsap from 'gsap'
import Link from 'gatsby-plugin-transition-link'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import TransitionLink, { TransitionPortal } from 'gatsby-plugin-transition-link'
import DisplayState from '../components/DisplayState'
import Layout from '../components/layout'


class Index extends Component {
  constructor(props) {
    super(props)

    this.verticalAnimation = this.verticalAnimation.bind(this)

    this.layoutContents = React.createRef()
    this.transitionCover = React.createRef()
  }

  verticalAnimation = ({ length }, direction) => {
    const directionTo = direction === 'up' ? '-100%' : '100%'
    const directionFrom = direction === 'up' ? '100%' : '-100%'

    // convert ms to s for gsap
    const seconds = length

    return gsap.timeline()
      .set(this.transitionCover, { y: directionFrom })
      .to(this.transitionCover, {
        y: '0%',
        ease: "power1.easeInOut",
        duration: seconds / 2,
      })
      .set(this.layoutContents, { opacity: 0 })
      .to(this.transitionCover, {
        y: directionTo,
        ease: "power1.easeIn",
        duration: seconds / 2,
      })
  }

  test(entry, node) {
    return gsap.from(
      node.querySelectorAll('h2, p, a, pre'),
      {
        opacity: 0,
        y: '+=50',
        duration: 1,
        stagger: 0.1,
      },
    )
  }

  message(message) {
    console.log(message)
  }

  render() {
    return (
      <Layout theme="white">
        <section ref={n => (this.layoutContents = n)}>
          <h1>Hi peoples</h1>
          <p>Check out these sick transitions.</p>
          <AniLink
            fade
            to="/student-signup"
            state={{
              modal: true
            }}
          >
          Student Signup fade
          </AniLink>
          <br />
          <Link
            to="/student-signup"
            data-backdrop="false"
            state={{
              modal: true
            }}
          >
          Student Signup plain
          </Link>
          <br />
          <AniLink
            paintDrip
            to="/student-signup"
            state={{
              modal: true
            }}
          >
          Student Signup paintDrip
          </AniLink>
          <br />
          <AniLink
            swipe
            direction="down"
            to="/student-signup"
            state={{
              modal: true
            }}
          >
          Student Signup swipe down
          </AniLink>
          <br />
          <AniLink
            swipe
            direction="up"
            to="/student-signup"
            state={{
              modal: true
            }}
          >
          Student Signup swipe up
          </AniLink>
          <br />
          <AniLink
            swipe
            direction="right"
            to="/student-signup"
            state={{
              modal: true
            }}
          >
          Student Signup swipe right
          </AniLink>
          <br />
          <AniLink
            swipe
            direction="left"
            to="/student-signup"
            state={{
              modal: true
            }}
          >
          Student Signup swipe left
          </AniLink>
          <br />
          <TransitionLink
            to="/student-signup"
            exit={{
              length: 1,
              trigger: ({ exit }) => this.verticalAnimation(exit, 'down'),
              state: { test: 'exit state' },
            }}
            entry={{
              delay: 0.5,
              trigger: ({ entry, node }) => this.test(entry, node),
            }}
          >
            Go to student signup that way{' '}
            <span aria-label="pointing up" role="img">
              👇
            </span>{' '}
            and animate in the next page
          </TransitionLink>
          <br />
          <br />
          <AniLink fade to="/page-2">
            Fade to page 2
          </AniLink>
          <br />
          <AniLink state={{ to: 'page2' }} to="/page-2">
            Go to page 2 normally
          </AniLink>
          <br />
          <AniLink cover to="/page-2" direction="right">
            Go to page 2 with a cover right
          </AniLink>
          <br />
          <AniLink paintDrip to="/page-2#test" hex="#4b2571">
            Go to page 2 with a paint drip
          </AniLink>
          <br />
          <TransitionLink
            to="/page-2"
            exit={{
              length: 1,
              trigger: ({ exit }) => this.verticalAnimation(exit, 'down'),
              state: { test: 'exit state' },
            }}
            entry={{
              delay: 0.5,
              trigger: ({ entry, node }) => this.test(entry, node),
            }}
          >
            Go to page 2 that way{' '}
            <span aria-label="pointing up" role="img">
              👇
            </span>{' '}
            and animate in the next page
          </TransitionLink>
          <br />
          <TransitionLink
            to="/page-2"
            exit={{
              length: 1.2,
              trigger: ({ exit }) => this.verticalAnimation(exit, 'up'),
            }}
            entry={{ delay: 0.5, length: 1, state: { layoutTheme: 'dark' } }}
          >
            Go to page 2 that way{' '}
            <span aria-label="pointing up" role="img">
              ☝️
            </span>
            and give us a dark theme when we get there.
          </TransitionLink>
          <br />
          <AniLink to="/page-2" onClick={() => console.log('Link clicked')}>
            Go to page 2 normally (with an `onClick`)
          </AniLink>
          <br />
          <Link to="/page-2">Go to page 2 with gatsby-link</Link>
          <DisplayState />
        </section>
        <TransitionPortal>
          <div
            ref={n => (this.transitionCover = n)}
            style={{
              position: 'fixed',
              background: '#4b2571',
              top: 0,
              left: 0,
              width: '100vw',
              height: '100vh',
              transform: 'translateY(100%)',
            }}
          />
        </TransitionPortal>
      </Layout>
    )
  }
}


export const fluidImage = graphql`
  fragment fluidImage on File {
    childImageSharp {
      fluid(maxWidth: 1000) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`;

export const pageQuery = graphql`
  query {
    teacher: file(relativePath: { eq: "teacher.png" }) {
      ...fluidImage
    }
    logo: file(relativePath: { eq: "logo.png" }) {
      ...fluidImage
    }
  }
`

export default Index
