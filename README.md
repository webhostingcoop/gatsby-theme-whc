# gatsby-theme-whc

Gatsby theme for the Webhosting.coop

## Development Notes

First, if you have not, you should follow the official gatsby 
[tutorial](https://www.gatsbyjs.com/tutorial/)
to ensure you have everything installed and get a general introduction to GatsbyJS.

Next observe the accessibility 
[page](https://www.gatsbyjs.com/docs/making-your-site-accessible/)
on the gatsby docs, all pages here at the co-op should be fully accessible.

Then, to catch up with where I am at here, I merely followed the
[official builtding a theme tutorial](https://www.gatsbyjs.org/tutorial/building-a-theme/)
then got transitions working and started experimenting with 'namespacing' in
node package management, oreilly has a nice 
[article on this subject](https://www.oreilly.com/library/view/learning-javascript-design/9781449334840/ch13s15.html).

As a result, there are 3 main workspaces for the theme 'gatsby-theme-whc'
'@webhostingcoop/gatsby-theme-whc' and
'@webhostingcoop/gatsby-theme-whc-dev',
The first of which was created along with the tutorial above, and the
other two just to see where all I needed to change stuff to make the org
namespace work out
then there are three testing sites site nssite and devsite for testing
out the three themes locally `yarn workspace devsite develop` to run the
devsite locally
there is a makefile for the lazy typist (me)

```
make s
make d
make n
```

or if you prefer verbosity:

```
make site
make dev
make namespacesite
```


## biblio

https://www.gatsbyjs.org/docs/adding-page-transitions-with-plugin-transition-link/#predefined-transitions

## Signing

Please sign your commits.  [Official git docs on signing](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work)
